

#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>
#include <stdint.h>
#include <math.h>
#include <avr/boot.h>
#include <EEPROM.h>
#include <avr/wdt.h>
#include <avr/sleep.h>

#include "athome_firmware.h"
#include "photo_measure.h"
#include "data_io.h"
#include "defs.h"

U8G2_SH1106_128X64_NONAME_1_4W_HW_SPI u8g2(U8G2_R0, 10, 4);

float coeffs[] = {0, 0, 0};
uint16_t cutoffs[] = {0, 0, 0, 0};

float temp = 0;
float setpoint = 0;
float integ = 0;
float dirs[8] = {0};
float dirs_sum = 0;
int dirs_idx = 0;

float P = 6;
float I = 0.8;
float I_ACCU = 20;
float D = 8;
float I_LIM = 3500;

int it = 0;
//boolean run_heater = false;
unsigned long start = 0;
unsigned long stop_time = 0;
uint16_t val = 0;
boolean run_fan = false;
float voltage = 0;
unsigned int adcgain = 2;

int state = STATE_IDLE;
unsigned long durations[4] = {0ul, 5ul * 60 * 1000, 36ul * 60 * 1000, 0ul};

int16_t data_adc[4] = {0};
int16_t data_adc_buf[16][4] = {0};

unsigned long last_temp = 0;
unsigned long last_adc = 0;
unsigned long last_lcd = 0;
int adcch = 0;
boolean rbutton_prev = false;
boolean dual_board = false;
boolean block_led = false;

void main_a()
{
  MCUSR = 0;
  wdt_disable();

  Serial.begin(115200);
  Serial.println("init.");
  Serial.print("C,fw:");
  Serial.println(VERSION);
  Serial.print("C,id:");
  for(int i=0x0E; i<=0x17; i++){
    Serial.printf("%02X",boot_signature_byte_get(i));
  }
  Serial.println();

  uint16_t flag;

  flag = 0;
  EEPROM.get(0, flag);
  EEPROM.get(2, coeffs[0]);
  EEPROM.get(6, coeffs[1]);
  EEPROM.get(10, coeffs[2]);
  Serial.print("C,tempcal:");
  Serial.println(flag == 0xABCD);
  flag = 0;
  EEPROM.get(14, flag);
  EEPROM.get(16, cutoffs[0]);
  EEPROM.get(18, cutoffs[1]);
  EEPROM.get(20, cutoffs[2]);
  EEPROM.get(22, cutoffs[3]);
  Serial.print("C,photocal:");
  Serial.println(flag == 0x1234);

  u8g2.begin();
  Wire.begin();

  analogReference(EXTERNAL);
  pinMode(HEATER, OUTPUT); // heater
  pinMode(2, OUTPUT);
  pinMode(UV_LED, OUTPUT);
  pinMode(7, OUTPUT); //Blue Led
  pinMode(8, INPUT);  // Button
  pinMode(9, OUTPUT); // Green Led
  pinMode(16, OUTPUT);
  digitalWrite(7, HIGH);
  digitalWrite(UV_LED, LOW);
  digitalWrite(HEATER, LOW); // heater
  digitalWrite(2, LOW);

  Wire.beginTransmission(0x48);
  Wire.write(byte(0x01)); //config
  int result = Wire.endTransmission();

  unsigned int photoboards = 0;
  if (result == 0)
  {
    photoboards++;
  }
  else
  {
    //Serial.print("ADC Error: ");
    //Serial.println(result);
  }

  Wire.beginTransmission(0x49);
  Wire.write(byte(0x01)); //config
  result = Wire.endTransmission();

  if (result == 0)
  {
    // dual-board mode
    photoboards++;
    dual_board = true;
  }
  Serial.print("C,photo:");
  Serial.println(photoboards);
  Serial.print("C,adcgain:");
  Serial.println(adcgain);

  // enable watchdog!
  MCUSR = 0; 
  wdt_enable(WDTO_1S);


  unsigned int hcounter = 0;
  unsigned long buttonpress_last = 0;
  unsigned int state_last = 999;
  unsigned long measure_trigger_delay = 0;
  while (1)
  {

    wdt_reset(); // needs to be called every second to prevent reset

    digitalWrite(9, (state == STATE_REACT1 || state == STATE_REACT2) && !block_led );
    digitalWrite(7, !block_led );

    // fan pwm
    if (run_fan)
    {
      //PORTC ^= _BV(2);
      PORTC |= _BV(2);
    }
    else
    {
      PORTC &= ~_BV(2);
    }

    //static int adcgain = 0;
    if (Serial.available() >= 2)
    {
      char c = Serial.read();
      if (c == '~')
      {
        // configure ADC gain
        // Serial.print("set gain:");
        adcgain = (Serial.read() - 48) & 0x7;
        Serial.print("C,adcgain:");
        Serial.println(adcgain);
      }
      else if (c == '!')
      {
        // begin ADC read
        task_adc_pulse(true, Serial.read() == '!' );
      }
      else if ( c == '@')
      {
        // self reset
        MCUSR = 0; 
        wdt_disable();
        wdt_enable(WDTO_15MS);
        while(1);
      }
      else if ( c == '*')
      {
        // calibration mode.
        MCUSR = 0;
        wdt_disable();
        digitalWrite(HEATER, LOW); // disable heater just in case.
        
        u8g2.firstPage();
        do
        {
          u8g2.setFont(FONT);
          u8g2.drawStr(36, 39, "CAL");
        } while (u8g2.nextPage());
        
        Serial.println("cal.");

        for(int i=0; i<48; i++){
          Serial.write(EEPROM.read(i%24));
        }

        while (Serial.read() != -1);
        for(int i=0; i<24; i++){
          while(!Serial.available());
          EEPROM.write(i, Serial.read());
        }

        for(int i=0; i<24; i++){
          Serial.write(EEPROM.read(i));
        }
        // reboot
        MCUSR = 0; 
        wdt_enable(WDTO_1S);
        while(1);
      }
      else if ( c == '^'){
        // temp calibration measurement mode.
        MCUSR = 0;
        wdt_disable();
        digitalWrite(HEATER, LOW); // disable heater just in case.   
        while(1){
          val = 0;
          for(int i=0; i< 32; i++){
            val += analogRead(A0);
          }
          Serial.println(val);
          delay(200);
        }    
      }
      while (Serial.read() != -1);
    }

    boolean h = digitalRead(8);
    if (rbutton_prev && !h) // activate on falling edge
    {
      delay(6); // debounce

      // increment state only on a short press
      if( buttonpress_last > 0)state = state + 1;

      if (state > 3)
        state = STATE_IDLE;
      start = millis();
      stop_time = durations[state] + millis();
      buttonpress_last = 0;
    }
    if( !rbutton_prev && h)
    {
      delay(6); //debounce
      buttonpress_last = millis();
    }
    if( buttonpress_last > 0 && (millis() - buttonpress_last) > 1000){
      // button held down for >1s
      state = STATE_MEASURE;
      buttonpress_last = 0;
    }
    rbutton_prev = h;

    if (millis() - last_temp > 100)
    {
      last_temp = millis();
      task_temp();
      task_print_A();
    }

    boolean done = task_adc_pulse(false);
    if (done)
    {
      task_print_B();
      analyze(cutoffs);
      set_display(true);
    }

    if (millis() - last_lcd > 500)
    {
      last_lcd = millis();
      task_update_lcd();
    }

    if( state != state_last && state == STATE_MEASURE){
      // measurement has been trigger by hardware
      set_display(false);
      measure_trigger_delay = millis() + 2000;
      
    }
    state_last = state;

    if( state == STATE_MEASURE && measure_trigger_delay != 0 && millis() > measure_trigger_delay){
      task_adc_pulse(true,true); // run measurement
      measure_trigger_delay = 0;
    }

    if ( state == STATE_IDLE && voltage < 6.4){
      lowbatt(false);
    }else if( voltage < 6){
      lowbatt(true);
    }

    //delay(5);
  }
}

void task_temp()
{

  if (state == STATE_REACT1)
  {
    switch (int(ceil((millis() - start) / 60E3)))
    {
    case 0 ... 5:
      setpoint = 95;
      break;
    default:
      setpoint = 0;
      break;
    }
  }
  else if (state == STATE_REACT2)
  {
    switch (int(ceil((millis() - start) / 60E3)))
    {
    case 0 ... 30:
      setpoint = 63;
      break;
    case 31 ... 35:
      setpoint = 95;
      break;
    case 36 ... 37:
      setpoint = 0;
      run_fan = true;
      break;
    default:
      setpoint = 0;
      run_fan = false;
      break;
    }
  }else{
    setpoint = 0;
    run_fan = false;
  }

  voltage = analogRead(A1) / 101.63;

  val = 0;
  for (int i = 0; i < 32; i++)
  {
    val += analogRead(A0);
  }

  temp = (coeffs[0]) * val * val + (coeffs[1]) * val + (coeffs[2]);
  if( !isfinite(temp) || temp>9999 ){
    temp = 9999; // sanity check
  }

  int power = 0;
  if (state != STATE_IDLE)
  {
    integ += I_ACCU * (setpoint - temp);
    if (integ > I_LIM)
      integ = I_LIM;
    if (integ < 0)
      integ = 0;

    int next = (dirs_idx + 1) % 8;
    dirs_sum = setpoint - temp - dirs[next];
    dirs[next] = setpoint - temp;
    dirs_idx = next;
    power = int(max(0.0, min(POWER_MAX, P * (setpoint - temp) + I * (integ / 100) + D * (10 * dirs_sum))));
  }
  analogWrite(HEATER, power);

}


void lowbatt( boolean disablescreen )
{
  digitalWrite(HEATER, LOW); // disable heater
  digitalWrite(UV_LED, LOW); // disable illimunator
  digitalWrite(7, LOW); //Blue Led
  digitalWrite(8, LOW);  // Button
  digitalWrite(9, LOW); // Green Led

  u8g2.firstPage();
  do
  {
    u8g2.setDrawColor(1);
    u8g2.setFont(FONT);
    u8g2.drawStr(14, 39, "LOW BATT");
  } while (u8g2.nextPage());

  MCUSR = 0;
  wdt_disable();
  cli();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  while(1)sleep_mode();

}