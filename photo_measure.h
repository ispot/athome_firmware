#ifndef PHOTO_MEASURE_HEADER
#define PHOTO_MEASURE_HEADER

#include <Arduino.h>

void task_req_adc(int adcch, int adcgain);
void task_read_adc(int adcch);
boolean task_checkdone_adc();
boolean task_adc_pulse(boolean reset, boolean use_uv=true);

#endif