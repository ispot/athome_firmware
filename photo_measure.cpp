
#include <Arduino.h>
#include <Wire.h>
#include "photo_measure.h"
#include "defs.h"



extern int16_t data_adc[4];
extern int16_t data_adc_buf[16][4];
extern unsigned int adcgain;
extern boolean dual_board;
extern boolean block_led;

/*
 * adcch: channel 0,1,2,3
 * adcgain: gain 0,1,2,3,4 corresponds to 1x,2x,4x,8x,16x
 * 
 */
void task_req_adc(int adcch, int adcgain)
{
  uint8_t adc = 0x48;
  if( dual_board && adcch >= 2){
    adc = 0x49; // switch to secondary mode
    adcch -= 2 ; // shift back by 2 for secondary
  }
  Wire.beginTransmission(adc);

  Wire.write(byte(0x01));
  Wire.write(byte(CONFIG | (adcch << 4) | ((adcgain + 1) << 1)));
  Wire.write(byte(CONFIG2));
  Wire.endTransmission();
}

void task_read_adc(int adcch)
{
  uint8_t adc = 0x48;
  if( dual_board && adcch >= 2){
    adc = 0x49; // switch to secondary mode
  }
  Wire.beginTransmission(adc);

  Wire.write(byte(0x00));
  Wire.endTransmission();

  while (Wire.available())
    Wire.read();
  Wire.requestFrom(adc, 2);
  int16_t d = Wire.read() << 8;
  data_adc[adcch] = d | Wire.read();
}

/*
 * returns true if done, false otherwise
 */
boolean task_checkdone_adc(int adcch)
{
  uint8_t adc = 0x48;
  if( dual_board && adcch >= 2){
    adc = 0x49; // switch to secondary mode
  }
  Wire.beginTransmission(adc);
  Wire.write(byte(0x01));
  Wire.endTransmission();

  while (Wire.available())
    Wire.read();
  Wire.requestFrom(adc, 2);
  boolean d = ((unsigned short)Wire.read()) >> 7 == 1;
  Wire.read();
  return d;
}

boolean task_adc_pulse(boolean reset, boolean use_uv)
{

  static unsigned long led_timer = 0;
  static unsigned short adc_ch = 0;
  static unsigned short cycle = 0;
  static boolean requested = false;
  if (reset)
  {
    led_timer = millis();
    adc_ch = 0;
    cycle = 0;
    requested = false;
    digitalWrite(UV_LED, use_uv);
    block_led = true;
  }
  if (led_timer == 0)
    return false; // not running
  if (millis() - led_timer < 100)
    return false; // warmup for 100ms
  if (millis() - led_timer > 2500)
  { // should never happen, but will prevent overheating
    digitalWrite(UV_LED, LOW);
    block_led = false;
    led_timer = 0;
    Serial.println("ADC timeout error!");
    return false;
  }
  if (!requested)
  {
    task_req_adc(adc_ch, adcgain);
    requested = true;
    return false;
  }

  if (task_checkdone_adc(adc_ch))
  {
    task_read_adc(adc_ch);
    data_adc_buf[cycle][adc_ch] = data_adc[adc_ch];

    requested = false;
    adc_ch = (adc_ch + 1) % 4;
    if (adc_ch == 0)
      cycle++;
  }

  if (cycle > 15)
  { // done
    digitalWrite(UV_LED, LOW);
    block_led = false;
    led_timer = 0;
    return true;
  }

  return false; // data not ready
}
