#ifndef DATA_IO_HEADER
#define DATA_IO_HEADER

void set_display(boolean s);
void task_print_A();
void task_print_B();
void task_update_lcd();
void task_update_lcd_measure();
void analyze(uint16_t* cutoffs);


#endif