#ifndef DEFS_HEADER
#define DEFS_HEADER

//define configureable full scale
#define CONFIG 0b11000001

// config 16 SPS Mode
//#define CONFIG2 0b00100011

// config 64 SPS Mode
#define CONFIG2 0b01100011


#define FAN 16
#define UV_LED 6
#define HEATER 5

// led plugged into fan temporarily
//#define UV_LED 16

#define STATE_IDLE 0
#define STATE_REACT1 1
#define STATE_REACT2 2
#define STATE_MEASURE 3

#define POWER_MAX 50.0
#define FONT u8g2_font_helvR14_tr

#define VERSION 26

#endif