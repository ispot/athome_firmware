
#include <U8g2lib.h>
#include <Arduino.h>
#include "data_io.h"
#include "defs.h"
#include "math.h"

extern U8G2_SH1106_128X64_NONAME_1_4W_HW_SPI u8g2;
extern float temp;
extern float setpoint;
extern float start;
extern float voltage;
extern int state;
extern int16_t data_adc_buf[16][4];
extern unsigned long stop_time;

static boolean display_results = false;
void set_display(boolean s){
  display_results = s;
}

uint8_t result = 0;
void analyze(uint16_t* cutoffs){
  
  float ch1_mean = 0;
  float ch2_mean = 0;
  for (int i = 0; i < 16; i++)
  {

    ch1_mean += data_adc_buf[i][1];
    ch2_mean += data_adc_buf[i][0];
    /*
    ch3_mean += data_adc_buf[i][3];
    ch4_mean += data_adc_buf[i][2];
    */
  }
  ch1_mean /= 16.0;
  ch2_mean /= 16.0;

  float ch1_std = 0;
  float ch2_std = 0;
  for (int i = 0; i < 16; i++)
  {

    ch1_std += (ch1_mean - data_adc_buf[i][1])*(ch1_mean - data_adc_buf[i][1]);
    ch2_std += (ch2_mean - data_adc_buf[i][0])*(ch2_mean - data_adc_buf[i][0]);
  } 
  ch1_std = sqrt(ch1_std/16);
  ch2_std = sqrt(ch2_std/16);

  // 0:unknown, 1:inconclusive, 2:positive, 3:negative
  if( ch1_mean < cutoffs[0] && ch2_mean < cutoffs[1]){
    result = 3;
  }else if( ch1_mean > cutoffs[2] && ch2_mean > cutoffs[3]){
    result = 2;
  }else{
    result = 1;
  }

  // debug output
  Serial.printf("C,result:%u,%i,%i,%i,%i\n",result,(int)ch1_mean,(int)ch2_mean,(int)ch1_std,(int)ch2_std);

}

void task_print_A()
{

  Serial.print("A,");
  Serial.print(temp);
  Serial.print(",");
  Serial.print(setpoint);
  Serial.print(",");
  Serial.print(state);
  Serial.print(",");
  Serial.print(millis() - start);
  Serial.print(",");
  Serial.print(voltage);
  Serial.println("");
}

void task_print_B()
{
  for (int i = 0; i < 16; i++)
  {
    Serial.print("B,");
    Serial.print(data_adc_buf[i][1]);
    Serial.print(",");
    Serial.print(data_adc_buf[i][0]);
    Serial.print(",");
    Serial.print(data_adc_buf[i][3]);
    Serial.print(",");
    Serial.print(data_adc_buf[i][2]);
    Serial.println("");
  }
}

void task_update_lcd()
{

  if (millis() < 3000)
  {
    char a[16] = {0};
    sprintf(a, "%d", VERSION);
    u8g2.firstPage();
    do
    {
      u8g2.drawDisc(64,32,30, U8G2_DRAW_ALL);
      u8g2.setDrawColor(0);
      u8g2.drawDisc(64,32,27, U8G2_DRAW_ALL);
      u8g2.drawBox(0,32-11,127,22);
      u8g2.setDrawColor(1);

      u8g2.setFont(FONT);
      u8g2.drawStr(36, 39, "iSPOT");
      
      u8g2.drawStr(0,62, a);

    } while (u8g2.nextPage());
    return;
  }

  if( state == STATE_MEASURE){
    task_update_lcd_measure();
    return;
  }

  static boolean blink = 0;
  blink ^= 1;

  u8g2.firstPage();

  unsigned long m = millis();
  unsigned long minutes = (stop_time - m) / (60ul * 1000);
  unsigned long seconds = ((stop_time - m) - (minutes * (60ul * 1000))) / 1000;
  if (m > stop_time)
  {
    minutes = 0;
    seconds = 0;
  }
  char a[16] = {0};
  char b[16] = {0};
  char c[16] = {0};

  if (m < stop_time || blink)
  {
    sprintf(a, "%02lu:%02lu", minutes, seconds);
  }

  sprintf(b, "%03u.%02u", int(temp), int((temp - int(temp)) * 100));

  switch (state)
  {
  case STATE_IDLE:
    sprintf(c, "idle");
    break;
  case STATE_REACT1:
    sprintf(c, "reaction 1");
    break;
  case STATE_REACT2:
    sprintf(c, "reaction 2");
    break;
  }

  //sprintf(a, "Temp: %03u.%02u", int(temp), int((temp-int(temp))*100));
  //sprintf(b, "t: %02lu:%02lu", minutes, seconds);
  do
  {
    char d[16] = {0};
    u8g2.setFont(FONT);
    u8g2.drawStr(0, 24, a);
    int len = u8g2.drawStr(54, 24, b);

    u8g2.setFont(u8g2_font_unifont_t_symbols);
    int len2 = u8g2.drawGlyph(len + 54, 22, 0x00b0);
    u8g2.setFont(FONT);
    u8g2.drawStr(len + len2 + 54, 24, "c");

    u8g2.drawStr(0, 50, c);
  } while (u8g2.nextPage());
}

void task_update_lcd_measure(){

  u8g2.firstPage();
  if( display_results ){

    char a[16] = {0};
    char b[16] = {0};

    char res_types[][16] = {"UNKNOWN","INCONC.","POSITIVE","NEGATIVE"};
    sprintf(a, "RESULT");
    if( result >= 4 )result = 0; // ??
    sprintf(b, res_types[result]);

    do
    {
      u8g2.setDrawColor(1);
      u8g2.drawBox(0,0,128,64);
      u8g2.setFont(FONT);
      u8g2.setDrawColor(0);
      u8g2.drawStr(63-(u8g2.getStrWidth(a)/2), 24, a);
      u8g2.drawStr(63-(u8g2.getStrWidth(b)/2), 50, b);
      u8g2.setDrawColor(1);
    } while (u8g2.nextPage());

  }else{


    do
    {
      u8g2.setDrawColor(1);
      u8g2.setFont(FONT);
      u8g2.drawStr(36, 39, "WAIT");

    } while (u8g2.nextPage());

  }


}
